'use strict';


import ScrollReveal from 'scrollreveal';

/*Variables*/
const header = document.querySelector('header');
/*timer function variable*/
const timerText = document.querySelector('.claim_box--expires span');
let seconds = 300;
/*ingredients variable*/
const ingredientsPopupItem = document.querySelectorAll('.ingredients_list--popup');
const ingredientsBtns = document.querySelectorAll('.ingredients_more--btn');
const ingredientsCloseBtns = document.querySelectorAll('.ingredients_more--btn_close');
/*product variables*/
const amountWrap = document.querySelector('.product_buy--amount');
/*popup variables*/
const loginBtn = document.querySelectorAll('.login');
const popupLogin = document.querySelector('.popup_login');
const popupClose = document.querySelectorAll('.popup_close');
const popupArr = document.querySelectorAll('.popup');
const signupBtn = document.querySelectorAll('.sign_up');
const popupSignup = document.querySelector('.popup_sign');
const trialBtn = document.querySelectorAll('.start_trial');
const popupTrial = document.querySelector('.popup_trial');
/*mobile menu variables*/
const burger = document.querySelector('.burger');
const mobileMenu = document.querySelector('.mobile_menu');


/*Functions*/

/*mobile menu*/
burger.addEventListener('click', (e) => {
    mobileMenu.classList.toggle('active');
    document.body.classList.toggle('hidden');
})

/*popup login*/
if (loginBtn.length > 0) {
    for (let i = 0; i < loginBtn.length; i++) {
        loginBtn[i].addEventListener('click', (e) => {
            e.preventDefault();
            removePopups();
            popupLogin.classList.add('active');
        })
    }
}

/*popup sign up*/
if (signupBtn.length > 0) {
    for (let i = 0; i < signupBtn.length; i++) {
        signupBtn[i].addEventListener('click', (e) => {
            e.preventDefault();
            removePopups();
            popupSignup.classList.add('active');
        })
    }
}

/*popup trial*/
if (trialBtn.length > 0) {
    for (let i = 0; i < trialBtn.length; i++) {
        trialBtn[i].addEventListener('click', (e) => {
            e.preventDefault();
            removePopups();
            popupTrial.classList.add('active');
        })
    }
}

/*popup close*/
if (popupClose.length > 0) {
    for (let i = 0; i < popupClose.length; i++) {
        popupClose[i].addEventListener('click', (e) => {
            e.preventDefault();
            removePopups();
        })
    }
}

const removePopups = () => {
    if (popupArr.length > 0) {
        for (let p = 0; p < popupArr.length; p++) {
            popupArr[p].classList.remove('active');
        }
    }
}


/*product amount*/
if (amountWrap !== null) {
    const amountMinus = document.querySelector('.amount--minus');
    const amountNumber = document.querySelector('.amount--count');
    const amountPlus = document.querySelector('.amount--plus');
    let amountCount = 1;

    amountMinus.addEventListener('click', (e) => {
        e.preventDefault();
        if (amountCount >= 2) {
            amountCount--;
            amountNumber.innerHTML = amountCount;
        } else {
            amountCount = 1;
            amountNumber.innerHTML = amountCount;
        }
    })

    amountPlus.addEventListener('click', (e) => {
        e.preventDefault();
        amountCount++;
        amountNumber.innerHTML = amountCount;
    })
}

/*open popup on ingredients page*/
if (ingredientsPopupItem.length > 0) {
    ingredientsOpen();
}


function ingredientsOpen() {
    for (let i = 0; ingredientsBtns.length > i; i++) {
        ingredientsBtns[i].addEventListener('click', (e) => {
            e.preventDefault();
            ingredientsPopupItem[i].classList.add('active');
        })
    }
    for (let i = 0; ingredientsCloseBtns.length > i; i++) {
        ingredientsCloseBtns[i].addEventListener('click', (e) => {
            e.preventDefault();
            ingredientsPopupItem[i].classList.remove('active');
        })
    }
}

/*timer function on reasons page*/
function secondPassed() {
    let minutes = Math.round((seconds - 30) / 60),
        remainingSeconds = seconds % 60;

    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds;
    }

    timerText.innerHTML = minutes + ":" + remainingSeconds;
    if (seconds == 0) {
        clearInterval(countdownTimer);
        timerText.innerHTML = "00:00";
    } else {
        seconds--;
    }
}

if (timerText !== null) {
    let countdownTimer = setInterval(secondPassed, 1000);
}


/*header stick after scroll*/
if (header !== null) {
    document.addEventListener('scroll', (e) => {
        if (window.scrollY > 0) {
            header.classList.add('scroll');
        } else {
            header.classList.remove('scroll');
        }
    })
}

/*preloader*/
const preloader = document.querySelector('.preloader');

if (preloader !== null) {
    preloaderLoad(preloader, execution);
}

function execution() {
    animation();
}

function preloaderLoad(wrap) {
    wrap.classList.add('load');
    setTimeout(() => {
        wrap.classList.remove('active');
        execution();
    }, 1700);
}

function animation() {
    ScrollReveal().reveal('.up', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });

    ScrollReveal().reveal('.opacity', {
        distance: '0px',
        opacity: 0,
        interval: 700,
        duration: 3700,
        delay: 500,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });
}